@0xf6cbcc0f154f57bf;

using Common = import "common.capnp";
using HotelCommon = import "hotel_common.capnp";

struct ShopRequest {

  struct StarFilter {
    minStars @0 :Int32;
    maxStars @1 :Int32;
  }

  struct AmenityFilter {
    amenityCodes @0 :List(Text);
    amenityOperator @1 :Text = "AND";
  }

  struct Sort {

    enum Type {
      value @0;
      price @1;
      name @2;
      distance @3;
      stars @4;
      featured @5;
    }

    enum Direction {
      default @0;
      ascending @1;
      descending @2;
    }

    type @0 :Type = value;
    direction @1 :Direction = default;
  }

  location @0 :Common.SearchLocation;
  hotelIds @1 :List(UInt32);
  roomOccupancy @2 :List(Common.TravelerGroup);
  startDate @3 :Text;
  endDate @4 :Text;
  isDateless @5 :Bool = false;
  sort @6 :Sort;
  resultsPerPage @7 :Int32 = 25;
  page @8 :Int32 = 1;
  numPropertiesSeen @9 :Int32;
  salesModel @10 :Common.SalesModel;
  nameFilter @11 :Text;
  starRatingFilter @12 :StarFilter;
  brandFilter @13 :List(Text);
  amenityFilter @14 :AmenityFilter;
  promoCode @15 :Text;
  showFeatured @16 :Bool = false;
  opaqueTypeIndicator @17 :HotelCommon.OpaqueTypeIndicator = land;
  rateType @18 :HotelCommon.RateType;
  corporateInfoCode @19 :Text;
}

