@0xcdfd0cfb0126240f;

enum SalesModel {
  unspecified @0;
  payNow @1;
  payLater @2;
}

struct Child {
  age @0 :Int32;
}

struct Infant {
  age @0 :Int32;
  seated @1 :Bool;
}

struct TravelerGroup {
  adultCount @0 :Int32;
  seniorCount @1 :Int32;
  children @2 :Child;
  infants @3 :Infant;
}

struct Address {
  street1 @0 :Text;
  street2 @1 :Text;
  street3 @2 :Text;
  cityName @3 :Text;
  stateProvinceCode @4 :Text;
  countryCode @5 :Text;
  postalCode @6 :Text;
}

struct SearchLocation {
  enum Type {
    city @0;
    airport @1;
    poi @2;
    address @3;
    latlong @4;
    keyword @5;
    state @6;
    country @7;
    region @8;
    name @9;
  }
  type @0 :Type;
  name @1 :Text;
  cityName @2 :Text;
  stateProvinceCode @3 :Text;
  countryCode @4 :Text;
  iataCode @5 :Text;
  address @6 :Address;
  locationId @7 :UInt64;
  orbitzLocationId @8 :UInt32;
  cityId @9 :UInt32; # To support slog_facts
  latitude @10 :Float32;
  longitude @11 :Float32;
  radius @12 :Int32;
  radiusUnits @13 :Text = "mi";
  locationKeyword @14 :Text;
  modifier @15 :Text;
}

