require 'capn_proto'
require 'pbr'

module Hotel extend CapnProto::SchemaLoader
    load_schema("./capnp/hotel_shop_request.capnp")
end


pb = Hotel::ShopResponse.new_message
pb.location.type = 'keyword';
pb.hotelIds @1 :List(UInt32);
pb.roomOccupancy @2 :List(Common.TravelerGroup);
pb.startDate = "2015/09/04";
pb.endDate = "2015/09/05";
pb.sort @6 :Sort;
pb.numPropertiesSeen = 0;
pb.salesModel @10 :Common.SalesModel;
pb.starRatingFilter @12 :StarFilter;
pb.brandFilter @13 :List(Text);
pb.amenityFilter @14 :AmenityFilter;
pb.showFeatured @16 :Bool;
pb.opaqueTypeIndicator @17 :HotelCommon.OpaqueTypeIndicator = land;
pb.rateType @18 :HotelCommon.RateType;
pb.corporateInfoCode @19 :Text;
